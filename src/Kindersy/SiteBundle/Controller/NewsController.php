<?php

namespace Kindersy\SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class NewsController extends Controller
{

    private function getLatestPost() {
        $repository = $this->getDoctrine()->getRepository('KindersySiteBundle:Post');
        return $repository->findOneBy(array(), array('date' => 'DESC'));
    }

    private function getOlderPosts(\Kindersy\SiteBundle\Entity\Post $post) {

        $query = $this->getDoctrine()->getRepository('KindersySiteBundle:Post')->createQueryBuilder('p')
            ->where('p.date < :date')
            ->setParameter('date', $post->getDate()->format('Y-m-d'))
            ->orderBy('p.date', 'DESC')
            ->setMaxResults('3')
            ->getQuery();

        return $query->getResult();
    }

	/**
	 * @Template()
	 */
    public function indexAction()
    {

        $post = $this->getLatestPost();

        return array(
                    'page' => 'news',
                    'post' => $post,
                    'oldPosts' => $this->getOlderPosts($post),
                );
    }

    public function showAction($id)
    {
        $repository = $this->getDoctrine()->getRepository('KindersySiteBundle:Post');
        $post = $repository->findOneBy(array('id' => $id));
 
       return $this->render(
                'KindersySiteBundle:News:index.html.twig',
                array(
                    'page' => 'news',
                    'post' => $post,
//                    'oldPosts' => $this->getOlderPosts($post),
                )
            );
    }

	/**
	 * @Template()
	 */
    public function thumbAction()
    {
        return array(
                    'post' => $this->getLatestPost(),
                );
    }

	/**
	 * @Template()
	 */
    public function promoAction()
    {
        return array(
                    'post' => $this->getLatestPost(),
                );
    }
}
