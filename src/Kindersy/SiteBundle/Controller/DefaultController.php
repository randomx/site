<?php

namespace Kindersy\SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{

    public function showAction($page)
    {
        $repository = $this->getDoctrine()->getManager()->getRepository('KindersySiteBundle:Article');
        $article = $repository->findOneBy(array('page' => $page));

        $template = ($article && $article->getTemplate()) ? $article->getTemplate() : 'default';

        return $this->render('KindersySiteBundle:Default:' . $template . '.html.twig', 
                array(
                    'page' => $page,
                    'article' => $article,
                )
            );
    }
}
