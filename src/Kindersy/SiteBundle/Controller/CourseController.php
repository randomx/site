<?php

namespace Kindersy\SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class CourseController extends Controller
{

	/**
	 * @Template()
	 */
    public function indexAction()
    {
        $repository = $this->getDoctrine()->getRepository('KindersySiteBundle:Course');

        $courses = $repository->findAll();

        return array(
                    'page' => 'courses',
                    'courses' => $courses,
                );
    }

}
