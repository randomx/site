<?php

namespace Kindersy\SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class TestimonialsController extends Controller
{

	/**
	 * @Template()
	 */
    public function indexAction()
    {
        $repository = $this->getDoctrine()->getRepository('KindersySiteBundle:Testimonial');

        $testimonials = $repository->findAll();

        return array(
                    'page' => 'testimonials',
                    'testimonials' => $testimonials,
                );
    }

}
