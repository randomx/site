<?php

namespace Kindersy\SiteBundle;

class SiteConfig
{

    private $config = array();

    public function __construct($entityManager)
    {
        $repository = $entityManager->getRepository('KindersySiteBundle:Config');
        $_config = $repository->findAll();
        foreach ($_config as $config_entry) {
            $this->config[$config_entry->getName()] = $config_entry->getValue();
        }
    }

    public function get($key) {
        return $this->config[$key];
    }

}
